:- use_module(library(clpfd)).
%I recognize + cognize the event -> I recognize the event = Reslist

%time order : Textlist1 < Textlist2 -> a) exist at least a prefix in Textlist1 + at least a suffix in Textlist2
% + b) perhaps an Inter inbetween, smaller than min(|Textlist1|,|Textlist2|)
%So 2 cases (excluded tier): Alpha) No Inter
%                            Beta)  Inter = not(Alpha)) ... exists I, until end of Textlist1, from which Textlist1[I] = Textlist2[0], ...
%scotch(Textlist1,Textlist2,Reslist,Inter)
scotch_beta([H],[H], [H],[H])  :- % 1 elt in common
scotch_beta([H1|Rest1],[H2|Rest2],Reslist,) :-  Rest  #= Divided mod Divider, Rest #=< Divided, Divider #=< Divided .%others constraints are for inference lbl545547

is_divide(Divider,Divided) :- divide(Divider,Divided,0).

:- begin_tests(lists).
%:- use_module(library(lists)).
:- use_module(library(clpfd)).

test(div6by2) :-  divide(2,6,Rest).

:- end_tests(lists).

