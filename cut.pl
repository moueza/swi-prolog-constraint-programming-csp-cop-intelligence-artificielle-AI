:- use_module(library(prolog_debug)).%for spy four guitracer 

a().
b().


and(X,Y) :- (a(), b()).
andC(X,Y) :- (X, !, Y).

%  and(true,true).    and(true,truen).   andC(true,true).
%  and(true,false).   lbl54527           andC(true,false).
%  and(false,false).  and(false,falsen).   !true pas meme reaction que falsen

andN(X,Y) :- (a(), \+ b()).
spy(andN(X,Y)).
% guitracer. andN(true,false). : lbl54527

%Xand
%xand en fx des simples

%orN(false,true).
%orN(false,false).
