:- use_module(library(clpfd)).
 /*

   prefix AND sufix
    
   abCDE  CDEfg  -> abCDEfg


    
    abCDE    L1
      CDEfg  L2
    

    xorText( L1 , L2,    Prefix,And,Sufix,  Xor  )
   xorText( abCDE , CDEfg,  ab    ,CDE,fg   ,  abCDEfg  )

    Existe a,n max, cpt € [0;n-1 ] tq L2[cpt]  == L1[cpt+a]

  




Appel :  xorText( L1,L2,   A,N, 0       ,  Prefix,And,Sufix,  Xor  ), label(N) .
*/


 xorText( L1,L2,   A,N, N       ,  Prefix,And,Sufix,  Xor  ) .

 xorText( L1,L2,   A,N, Ncurrent,  Prefix,And,Sufix,  Xor  ) :-
      N2 #= N - 1,
      xorText( L1,L2,   A,N, Ncurrent,  Prefix,And,Sufix,  Xor  ) .

