:- use_module(library(clpfd)).
    %compiler
    functionn_plus(X,Y,Tot) :-     
      X in -10..10, %et non pas ins
      Y in -10..10,
      Tot #= (X-.5) * (X-.5)  + (Y-.5) * (Y-.5).
    %toplevel
    %functionn_plus(X,Y,Tot), labeling([min(Tot)],[X,Y]).
