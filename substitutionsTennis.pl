:- use_module(library(clpfd)).
%https://www.swi-prolog.org/pldoc/man?section=clpfd-integer-arith

%Hak
%    C^_ Comb, Arrangement
%play(J1,J2).
%play(J1,J2,NbJoeurs) :- integer(J1),integer(J2),between(1,NbJoeurs-1,J1), between(2,NbJoeurs,J2), J1<J2.
play(J1,J2,NbJoeurs) :- Subb #= NbJoeurs -1, between(1,Subb ,J1), between(2,NbJoeurs,J2), J1 #< J2.
%pool(NbJoeurs,NbMatchs) :- play(J1,J2).
%findall Bag NbSolutions = BagLength


%  play(J1,J2,4).
% findall(play(X,Y), play(J1,J2,4),Bag). KO
%  findall(play(X,Y), play(X,Y,4),Bag).
%  findall(play(X,Y), play(X,Y,4),Bag),length(Bag,L).
%  findall(play(X,Y), play(X,Y,6),Bag),length(Bag,L).  %15
%  findall(play(X,Y), play(X,Y,5),Bag),length(Bag,L).
%  findall(play(X,Y), play(X,Y,6),Bag).
