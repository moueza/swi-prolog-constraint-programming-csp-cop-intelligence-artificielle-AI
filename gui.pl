:- use_module(library(clpfd)).

%https://www.swi-prolog.org/pldoc/doc_for?object=sub_string/5
%https://www.swi-prolog.org/pldoc/man?section=string-predicates

%posOfChar(Str,Char,Pos) : 

posOfChn(Str,Seed,   Pos) :- sub_string(Str,Beta,N,After,Seed), Pos #= Beta.  %one-line

%posOfChar(Str,Char,Pos)
%  posOfChn("abc","b",Pos).  R: 1
%  posOfChn("abcb","b",Pos).    1 3
%  posOfChn(".","b",Pos).
%  posOfChn("     .   ",".",Pos).    5

/*  posOfChn("     .
",".",Pos).               */

/*string_chars("aze
|    ",C).
C = [a, z, e, '\n'].
*/

% string_chars("abc123",C).
%  string_chars(Chn,[a, z, e, '\n','1', '2', '3']).
%    posOfChn("aze\n123", "1",Pos).

coords(S,Seed,Pos,X,Y,NbLign) :-  posOfChn(S,Seed,Pos),
			   %  coords("aze\n123","1",Pos,X,Y).

				  %splitLineByLine
