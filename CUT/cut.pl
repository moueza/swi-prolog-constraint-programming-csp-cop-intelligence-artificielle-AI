%% https://book.simply-logical.space/part_i.html#other_uses_of_cut
%% notrace,nodebug.
    
likes(peter,Y):-friendly(Y),!.
likes(T,S):-student_of(S,T).
student_of(maria,peter).
student_of(paul,peter).
friendly(maria).
