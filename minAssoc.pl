%suba(A,B,R) :- R is  10 + 1 . %11 OK
suba(A,B,R) :- R is abs( A - B ).
suba(2,3,X).%1

is_element(A,[A]).
%is_element(1, []) :- false.
is_element(A,[B|L]) :-
    A \= B,
    is_element(A,L).
% is_element(3,[1,2,3]). -> true ?

squareSum([A],[B], (A - B) * (A - B)).
squareSum([A|R],[B|R2],Res) :-
    squareSum(R,R2,Res2 ),
    Res is (A - B) * (A - B) + Res2.

%squareSum([1,2,3],[1,2,3],X). %->0
%squareSum([1,2,3],[2,2,3],X) ->1
    
%minAssoc([A,B],Left1,Left2,SQDIFF) :-
%   (
%      is_element(  A ,Left1)
%   ;
%      is_element(  A ,Left2)
%   ),
%
%   (
%      is_element(  B ,Left1)
%   ;
%      is_element(  B ,Left2)
%   )
minAssoc([A,B],Left1,Left2,SQDIFF) :-
   squareSum([A],[B],SQDIFF),
   (
      minAssoc([A,B],[A|_],[B|_],SQDIFF)
   ;
      minAssoc([A,B],[B|_],[A|_],SQDIFF)
   ).

minAssoc([A,B|L],Left1,Left2,SQDIFF) :-
   squareSum([A],[B],SQDIFF),
   (
      minAssoc([A,B],[A|_],[B|_],SQDIFF)
   ;
      minAssoc([A,B],[B|_],[A|_],SQDIFF)
   ).

%minAssoc([0,0],[],[],0). -> 0?



%minAssoc([A,B|],[A|R],[B|R2]):- 



%minAssoc([1,5,1,5 ], FROM,TO,SQUARESum)=-
