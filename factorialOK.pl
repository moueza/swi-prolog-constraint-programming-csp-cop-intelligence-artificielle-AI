%https://www.swi-prolog.org/pldoc/man?section=clpfd-factorial
%comparator https://www.swi-prolog.org/pldoc/man?section=arith
%suba(A,B,R) :- R is  10 + 1 . %11 OK
suba(A,B,R) :- R is abs( A - B ).
suba(2,3,X).%1

    % A.9.4 https://www.swi-prolog.org/pldoc/man?section=clpfd
:- use_module(library(clpfd)).
    %https://youtu.be/sHo6-hk21L8?t=494
n_factorial(0, 1).
n_factorial(N, F) :-
    N #> 0,
  
        N1 #= N - 1,
        n_factorial(N1, F1),
        F #= N*F1.


