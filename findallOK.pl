%https://stackoverflow.com/questions/59247778/prolog-how-to-count-the-number-of-facts-without-using-a-built-in
    
stops(jubilee,bondstreet,1).
stops(jubilee,waterloo,2).
    stops(jubilee,bakerstreet,3).

    %Res : findall(1, stops(jubilee,_,_), List), length(List, Count).
    %https://www.swi-prolog.org/pldoc/doc_for?object=findall/3 ... notes
    %idem  findall(1, stops(_,_,_), List), length(List, Count).




    % findall(Y, stops(_,Y,_), List), length(List, Count).
