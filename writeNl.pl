:- use_module(library(clpfd)).
%++++ https://stackoverflow.com/questions/41453979/write-in-prolog-are-not-printing-the-variable-value
%from https://stackoverflow.com/questions/34082799/breadth-first-search-in-prolog
    
	     


feuille(0).
feuille(1).
feuille(2).
feuille(4).
% feuille(F).
link(0,1,2).   %(pere,fils G,fils D)
link(2,-1,4).
%linkify(link(Parent,FilsG,FilsD)) :- write(FilsG),linkify(link(FilsG,_,_)), write(FilsD),linkify(link(FilsD,_,_)).
%linkify(link(Parent,FilsG,FilsD)) :- write(FilsG),linkify(link(FilsG,_,_)).
%linkify(link(Parent,FilsG,FilsD)) :- write(FilsG),write(" "), write(G2), write(D2),linkify(link(FilsG,G2,D2)).
linkify(link(Parent,FilsG,FilsD)) :-  print(Parent),print(" "),print(FilsG),print(" "), print(FilsD),link(Parent,FilsG,FilsD).%TODO depth ; https://stackoverflow.com/questions/12923018/prolog-print-the-value-of-a-variable
%   [writeNl].
%link(P,G,D)
%   linkify(link(P,G,D)).
