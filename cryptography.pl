:- use_module(library(clpfd)).
%OK because number constraints for variables by this 1st directive

%divide(2,6)
%divide(A,Z)
divide(Divider,Divided,Rest) :-  Rest  #= Divided mod Divider, Rest #=< Divided, Divider #=< Divided .%others constraints are for inference lbl545547

is_divide(Divider,Divided) :- divide(Divider,Divided,0).

%decreasing
is_prime(1).
is_prime(2).
is_prime(N) :- Decr #= N-1, Dividers in 2..Decr, not(is_divide(Dividers,N)).%N, no n. in integers not expr

%P AND Q are primes from Cryptography field
primes_factorize(N,P,Q)  :- P #>= 2, Q #>= 2, P #=< N, Q #=< N,is_prime(P), is_prime(Q), N #= P * Q .

:- begin_tests(lists).
%:- use_module(library(lists)).
:- use_module(library(clpfd)).

test(div6by2) :-  divide(2,6,Rest).
test(div7by2) :-  divide(2,7,Rest).
test(div7by2inference) :-  divide(Divider,7,1). %2 or 6
% divide(Divider,7,1), Divider #=< 7. %lbl545547
%test(isprime6KO) :-
%    not(is_prime(6)).
test(div7by2bool) :-  not(divide(2,7,0)).

test(isprime2) :- is_prime(2).

test(is2divide3) :- not(is_divide(2,3)).
test(isprime3) :- is_prime(3).

test(isprime4KO) :- not(is_prime(4)).


%test(factorize2) :- primes_factorize(2,P,Q).
test(factorize4) :- primes_factorize(4,P,Q).

:- end_tests(lists).

% factorize 2 prime numbers
