%https://www.swi-prolog.org/pldoc/man?section=clpqr
    %:- use_module(library(clpfd)).
  :-  use_module(library(clpq)).
    %compiler

%toplevel
    %{ 6 * Y + 5 * X = 65},{ 7 * Y + 1 * X = 71}.
    %{ 4 * Y = 4}.
    %{ 6 * Y + 5 * X = 65, 7 * Y + 1 * X = 71}.
    
