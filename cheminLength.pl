%:- use_module(library(clpfd)).

:- use_module(library(clpr)).
    %cf arithStructureOK.pl
    
    %point(Id,X,Y).

    
    point(0,0,0).
    point(1,1,0).

    point(3,9,0).
    point(10,10,0).

    %start = id0, end= id10
    %lengthChem(CheminStart,CheminCurrent)
    %square(X,Res) :- Res  #= X * X.
    square(X,Res) :- Res  = X * X.

     %TODO init len = 0
    /* lengthChem([ point(Id1,X1,Y1), point(Id2,X2,Y2)],Len) :-  sqrt(square(X1-X2,S1)+(square(Y1-Y2,S2)),Len).*/
    lengthChem([ point(0,X1,Y1), point(10,X2,Y2)],Len) :-  square(X1-X2,S1),(square(Y1-Y2,S2)), Len = sqrt(S1+S2).
    /* lengthChem([ point(Id1,X1,Y1), point(Id2,X2,Y2)|Sub],Len) :-   lengthChem([ point(Id2,X2,Y2)|Sub],Len2) ,  Len #= Len2 + sqrt(square(X1-X2)+(square(Y1-Y2)).   % , = p08 */
     lengthChem([ point(Id1,X1,Y1), point(Id2,X2,Y2)|Sub],Len) :-   lengthChem([ point(Id2,X2,Y2)|Sub],Len2) ,  square(X1-X2,S1),(square(Y1-Y2,S2)) , Len = Len2 + sqrt(S1+S2).   % , = p08
									 

    
    /*cheminize
     lengthChem([ point(1,0,0), point(2,10,10)],0). 
     findall( point(Id,_,_), point(Id,_,_),Bag),lengthChem(Bag,Len).  */
