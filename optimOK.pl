:- use_module(library(clpfd)).
    %compiler
    functionn_plus(X,Y,Tot) :-     
      X in -10..9, %et non pas ins
      Y in -10..9,
      Tot #= (X-1) * (X-1)  + (Y-2) * (Y-2).
    %toplevel
    %functionn_plus(X,Y,Tot), labeling([min(Tot)],[X,Y]).
