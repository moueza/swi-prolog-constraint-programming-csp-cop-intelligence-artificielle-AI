:- use_module(library(clpfd)).



%https://twitter.com/moueza/status/1623071008021151747
    %abcdcabedcfg

    /*
    "Jean had 3 ones, and Jean wanted 4 others" -> "Jean "

    "abcdcabedcfg"->"ab" ;"dc"
    */

idem([],[]).
idem([H1|R1],[H2|R2]) :- length(R1,N1),length(R2,N2), idem(R1,R2), H1 #= H2.


% idem([1,2],[1,2]).    OK
%  idem([1,2],[1,2,3]). KO


% sub_string("abcdefgh",B,L,A,"efg").
%https://www.swi-prolog.org/pldoc/doc_for?object=sub_string/5			  

%dup(S,Alpha,Beta,N,Rsubst)
dup(S,Alpha,Beta,N,Rsubst) :- Alpha + (N-1) #< Beta, N #> 1 , sub_string(S,Alpha,N,A1,Rsubst),  sub_string(S,Beta,N,A2,Rsubst)./*idem() implicit by Rsubst :- Rsubst resolution Binding*/

/*"Jean had 3 ones, and Jean wanted 4 others" -> "Jean "   dup( "Jean had 3 ones, and Jean wanted 4 others",Alpha,Beta,N,Rsubst).
"abcdcabedcfg"->"ab" ;"dc"     dup( "abcdcabedcfg",Alpha,Beta,N,Rsubst).*/
%   dup( "Jean had 3 ones, and Jean wanted 4 others",Alpha,Beta,N,Rsubst).
%  dup( "Jea in Jea",Alpha,Beta,N,Rsubst).
%  dup( "Jea1 in Jea",Alpha,Beta,N,Rsubst).
%  dup( "Jea1 in Jea2",Alpha,Beta,N,Rsubst).

/*
N in common
*/




:- begin_tests(lists).
:- use_module(library(lists)).

test(idemTrue) :-
        idem([1,2],[1,2]).


test(idemFalse,[fail]) :-
        idem([1,2],[1,3]).


test(idemUnlength,[fail]) :-
        idem([1,2],[1,2,3]).

:- end_tests(lists).

			
