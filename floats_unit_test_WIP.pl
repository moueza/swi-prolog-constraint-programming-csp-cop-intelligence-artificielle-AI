%https://www.swi-prolog.org/pldoc/man?section=clpqr
    %:- use_module(library(clpfd)).
   % :-  use_module(library(clpq)).
:-  use_module(library(clpfd)).
:-  use_module(library(clpr)).
:-  use_module(library(error)). %types constraints
    %compiler

%toplevel
%3 #= 2 *  Y.

%Y = 0.5, Y = cos(X), float(X).

%point(X,Y,no).
point(0,0,0).
point(1,1,1).
point(2,2,2).
point(0,1,3).
point(2,2,4).
% point(_,_,_). ... true
% point(X,Y,No). : enumeration ... presence
% point(2,2,9). ... false
% point(2,2,),. ... f

%point(X,Y,No) :- point(X,Y,No).
%  point(2,2,_). %true ... true
%  point(2,10,_). %false not in data ... presence ... infinite loop ... recursive

point_presence(X,Y,No) :-  point(X,Y,_).
% point_presence(2,10,_).  %false not in data ... presence ... %infinite loop
% point_presence(2,2,_).    %true ... true presence++

    %cos(1.0)>0 . %true
    %X=1.0,cos(X)>0 . %X = 1.0.
%x=1.0,cos(x)>0 . %false
%    x=1.0,cos(x)>0.0 .  %false
%    x=1.0,cos(x)>0.0,float(x) .   %false
%     x=1.0,y=cos(x), y>0.0,float(x),float(y) .    %false
%    x=1.0, y#=cos(x), y#>0.0,float(x),float(y) . %KO
%    x#=1.0, y#=cos(x), y#>0.0,float(x),float(y) . 
%      X=1.0, Y = cos(X),label([Y]) .

%floats https://swi-prolog.discourse.group/t/correct-floating-point-arithmetic/5857/25?page=2
%float equals https://www.swi-prolog.org/search?for=+%3D%5C%3D

%X =:= 3.0 -2.0 * X. KO because X is boolean, not assignation

%X = 3.0 -2.0 * X, label([X]).

%X =@= 3.0 -2.0 * X. %

%X \== 3.0 -2.0 * X.

%X == 3.0 -2.0 * X.

% 1.0 = cos(Z).
% 1.0 #= cos(Z).

% X in 1..2,
%
%f(X,ResModfloat) :- Pi is 3.14, X #=< 2, X #>= 1, ProdFloat is X * Pi, ResModfloat is ProdFloat  - ceiling(ProdFloat).
f(X,Float,Float2,ProdFloat) :- X in 1..2,  Float is 3.1 , Float2 is 3.1 * 2.0,  ProdFloat =3.1 *  float(X) .

% f(X,Float,Float2,ProdFloat).
% f(X,Float,ProdFloat), labeling([X,ProdFloat ],[X,ProdFloat]).

%https://www.swi-prolog.org/pldoc/man?section=clpqr
% X = sin(Y).
% 1 = sin(Y).  % false
% {1 = sin(Y)}.  ++++ OK accolades
% 1.0 = sin(Y). 
% {1 = sin(Y), Y>2}.  false
% {1 = sin(2*Y)}. OK

%https://www.swi-prolog.org/pldoc/man?section=clpqr
distOpointEqPow(point(X,Y),Dist) :- Dist = pow( X*X + Y*Y, 0.5). %not all evaluated .. implicit .. expanded
% distOpointEqPow(point(3,3),Dist). 

distOpointIsPow(point(X,Y),Dist) :- Dist is pow( X*X + Y*Y, 0.5).
% distOpointIsPow(point(3,3),Dist). 

%   Dist is sqrt( X*X + Y*Y, 0.5). KO https://www.swi-prolog.org/pldoc/man?function=sqrt/1
%   Dist = sqrt( 3.0 + Y*Y, 0.5).  % OK not all evaluated (GPS)
%   { Dist = sqrt( 3.0 + Y*Y, 0.5) }. KO
%   { Dist is sqrt( 3.0 + Y*Y, 0.5) }. KO
%   { 4.2 is sqrt( 3.0 + Y*Y, 0.5) }. KO
%   { 4.2 = sqrt( 3.0 + Y*Y, 0.5) }. KO
%   {  sqrt( 3.0 + Y*Y, 0.5) is 4.2 }. KO
%   {  sqrt( 3.0 + Y*Y, 0.5) = 4.2 }. KO
%   { 1. >= X }, { X <= 10. }. KO
%   { 1.0 >= X }, { X <= 10.0 }. KO
%   { 1.0 >= X, X <= 10.0 }. KO
%   { 1.0 < X, X < 10.0 }.             OK !!!!!! pure inequalities monoorder
%                { 1.0 =< X, X <= 10.0 }. KO
%   { <=(1.0,X) , X <= 10.0 }. KO  https://www.swi-prolog.org/pldoc/man?section=clpqr
%   { <=(1.0,X)} ,{ X <= 10.0 }. KO
%   { (1.0 =< X)} ,{ X <= 10.0 }. KO
%             { 1.0 =< X} ,{ X <= 10.0 }. KO
%   { 1.0=<X, X<=10.0 }. KO
%   { X >= 1.0} ,{  10.0 >= X}.        OK !!!!!! equalities OK monoorder
%   { 1.0 =< X } , { X <= 10.0 }.  KO  
%   { 1.0 =< X } , { X =< 10.0 }.      OK !!!!!!!!!  reverse order test  monoorder .. so monoorder REQUIRED
%   { 0.5 =< Y } , { Y =< 1.0 } , { Y = sin(X) }. OK incomplete .. implicit .. cf Residual goals ( https://www.swi-prolog.org/pldoc/man?section=clpfd-residual-goals ) .. Core relations and search ( https://www.swi-prolog.org/pldoc/man?section=clpfd-search ) .. explicit .. label ( https://www.swi-prolog.org/pldoc/man?section=clpfd-search )
%  ... { 0.5 =< Y } , { Y =< 1.0 } , { Y = sin(X) }, label(X).  explicit demand  KO ( Error : "clpfd:labeling" so collect simplification not applicable for floats ) .. solution : attach float EQUALITIES only to integer for most of cases
%   
%   
%   
%   
%   
%   
%   
%   
%   
%   
%   
%   
%   
%   
%   
%   Dist = sqrt( 2.0 ).         OK implicit
%   { Dist = sqrt( 2.0 ) } . KO
%   Dist = { sqrt( 2.0 ) } .    OK implicit
%

distXYOEqSqrt(X,Y,Dist) :- { Dist = sqrt( X*X + Y*Y) } .
%  distXYOEqSqrt(1.0,1.0,Dist). KO
%  [floats].


distXYOIsSqrt(X,Y,Dist) :-  Dist is { sqrt( X*X + Y*Y) } .
% distXYOIsSqrt(1.0,1.0,Dist).   KO Arithmetic: {}/1 is not a function

distXYOIsSqrt2(X,Y,Dist) :-   { sqrt( X*X + Y*Y) } is Dist. 
%  distXYOIsSqrt2(1.0,1.0,Dist). KO




distXYOEqSqrt2(X,Y,Dist) :-  Dist = { sqrt( X*X + Y*Y) } .
%  distXYOEqSqrt2(1.0,1.0,Dist). OK implicit
%  distXYOEqSqrt2(1.0,1.0,Dist), label(Dist). KO   label ( https://www.swi-prolog.org/pldoc/man?section=clpfd-search ) .. clpfd .. so integers


distPointnoOEqSqrt(point(X,Y,No),Dist) :-  Dist = { sqrt( X*X + Y*Y) } .
%  point(_,_,No). OK
%  distPointnoOEqSqrt(point(_,_,No),Dist), label(No).

distPointnoOEqSqrt2(point(X,Y,No),Dist) :-  { Dist =:=  sqrt( X*X + Y*Y) } .
%
%   distPointnoOEqSqrt2(point(_,_,No),Dist). KO   Other way =:=





mixed(Integ, Floati) :- { 3.0 =:= Floati + 5.0 }, 6 #= 2*Integ . % combined 
%  mixed(Integ, Floati).     OK (because we have been explicit that Integ is integer with #= )
%  mixed(Integ, Floati),label(Integ).  KO clpfd:labeling




distPointnoOEqSqrt3(point(X,Y,No),Dist) :-  must_be(integer, No) , { Dist =:=  sqrt(  X*X + Y*Y) } .
%   distPointnoOEqSqrt3(point(_,_,No),Dist). KO



distPointnoOIsSqrt4(point(X,Y,No),Dist) :- No in 1..2 , { Dist is  sqrt(  X*X + Y*Y) } .
% distPointnoOIsSqrt4(point(_,_,No),Dist). KO  compound vs Expr



distPointnoOIsSqrt5(point(X,Y,No),Dist) :- No in 1..2 , Dist is { sqrt(  X*X + Y*Y) } .
% distPointnoOIsSqrt5(point(_,_,No),Dist). KO  compound vs Expr


%    { X > 1.0, Y = X + 2.5, Z = Y * 2.0, Z < 10.0 },    write(X), nl,   write(Y), nl,    write(Z), nl.


%{ X < 3.14, X > 0 , Y = cos(X) },    write(X), nl,   write(Y).

%{ X < 3.14, X > 0 , Y = sin(X) , Y = 1 },    write(X), nl,   write(Y). %egalite
%{ X < 6.28, X > 0 , Y = sin(X) , Y = 1 },    write(X), nl,   write(Y). % que 1 sol
% { X < 6.28, X > 0, Y = sin(X), Y = 1 },  write('X = '), write(X), nl,    write('Y = '), write(Y), nl,fail.   %pr toutes les sol par backtracking

										% findall(X, { X < 6.28, X > 0, Y = sin(X), Y = 1 }, Solutions), write('Solutions: '), write(Solutions).


%dist(x,y,z)
dist(X,Y,D) :- X * X + Y * Y.  

% (id,x,y,dist)
%pointfl(2., 2., 2.). % fail

pointfl(1, 1.0, 1.0).
pointfl(3, 3.0, 3.0).
pointfl(2, 2.0, 2.0).
%pointfl(Ref, _, _).
										%pointsfl_dist(pointfl(_, Xf, Yf),Dist)  :-  dist(Xf,Yf,Dist).		       

													   
{ U =< 10.0 , U >= -10.0 }.
% inf( U * U , Inf0).

% minimize(  U * U ).



min_floats(X, Y,X) :-  X =< Y . 
min_floats(X, Y,Y) :-  X >= Y .
%  { 3.0 >= 1.0 }. KO form
%   3.0 >= 1.0 .
% min_floats(3.0, 1.0,Minn).
:- begin_tests(min_floats310).
test(minOK) :- min_floats(3.0, 1.0,1.0).
test(minKO) :- not(min_floats(3.0, 1.0,99.0)).
:- end_tests(min_floats310).

min_of_floats_list([Minn], Minn).
min_of_floats_list([Head|Decimals], Minn) :-  min_of_floats_list(Decimals, Minn2),  min_floats(Head,Minn2,Minn)  .
% min_of_floats_list([2.0], Minn).
%  min_floats(5.0, 1.0, Minn).
% min_of_floats_list([5.0,  1.0],Minn).
% min_of_floats_list([5.0, 2.0, 1.0,4.0],Minn).
% use_module(library(clpr)).{ 20.0 = 5.0 * C }.   




:- begin_tests(min_of_floats_list2).
test(mofl2true) :-      min_of_floats_list([5.0,  1.0],1.0).
test(mofl2false) :- not(min_of_floats_list([5.0,  1.0],5.0)).
test(mofl2true029) :- min_of_floats_list([4.5886, 1.01,0.29,5.0,  1.0],0.29).
:- end_tests(min_of_floats_list2).


segment(point(X1,Y1),point(X2,Y2)).
segment(point(X1,Y1),point(X2,Y2),RefSeg).
segment(point(X1,Y1,Ref1),point(X2,Y2,Ref2)). 
segment(point(X1,Y1,Ref1),point(X2,Y2,Ref2),RefSeg).
segment(point(0,0),point(1,1),bissect1).
segment(point(1,0),point(0,1),bissect2).
% segment(point(1,0),point(0,1),RefSeg).  %true  :
% segment(point(1,0),point(1,1),RefSeg).  %false : not in data = presence ... KO
%                                                                  ||
%                                                                ground          
% segment(point(1,0),point(1,1),_).       %true  :                        ... true
% segment(_,_,_).                         %true  :                             
% segment(_,_,Ref).                       %true  :

%float intersection
%segments_intersection(Seg1,Seg2,PosIntersection)
segments_intersection(Bissect1,Bissect2,PosIntersection) :- segment(point(X1,Y1),point(X2,Y2),Bissect1),segment(point(X10,Y10),point(X20,Y20),Bissect2), Bissect1 \= Bissect2.
% segments_intersection(bissect1,bissect1,PosIntersection).  %false : idem
% segments_intersection(bissect1,bissect2,PosIntersection). %true
% segments_intersection(bissect1,bissect28,PosIntersection). %false : bissect28 not exist in data
% abc = abc. %true
% abc = abcd. %false
% abc \= abcd. %true
% abc \= abc. %false
