%infiniteNode=["/","*","-","+"]
    infiniteNode():- ("/";"*";"-";"+").
    % numbersOnceMax6=[1,2,5,7,13,25]
    %objectivePartial : 663
    %treeBin(Node,L,R)
    treeBin(RootNode,nil,nil).
    treeBin(RootNode,L,R) :- treeBin(L,L2,R2),treeBin(R,L3,R3).
  % treeBin(1,5,7).
%treeBin(1,treeBin(5,nil,nil),treeBin(7,nil,nil))
