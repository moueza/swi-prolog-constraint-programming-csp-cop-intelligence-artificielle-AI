%:- use_module(library(clpfd)).
%OK because number constraints for variables by this 1st direstive

%Matrix of costs ... cf Metanet https://github.com/moueza/isbn-0-8176-4009-6-engineering-and-scientific-computing-with-Scilab p112
%FRACTIONS ... resolution
%Uniform cost search ; A*
%https://www.youtube.com/watch?v=dRMvK76xQJI&ab_channel=JohnLevine
%automatic Dikstra 


%point (x,y,No6593)
%Point from		      
point(0,0,1).

point(1,1,2).
point(1,2,3).

point(10,9,4).
point(10,10,5). %Point end
%:same asbsisse

%edge(PointNoFrom,PointNoTo,No47854). TODO : automatic
edge(1,2).
edge(1,3).
edge(2,4).
edge(2,5).
edge(3,4).
edge(3,5).
%TODO scalar product x related

%ASC Xs like in functions 
is_edge_fx_ASC(E) :- E = edge(Point1,Point2), Point1 =  point(X1,_,_), Point2 =  point(X2,_,_), X1 #< X2.

%P80 of P99		
%path :- [edges(No47854)]
is_path([edge(From,To)]).
is_path([H|L]) :- edge(From,To),is_path(L).

/*better def*/
is_path2([edge(From,To)]).
is_path2([edge(From,To)|L]) :-  is_path(L).
%test :  WANTED obtained
/*test : is_path([edge(10,20)]).
true ;
false.
      true because in general, not from my data */

/*test : is_path([edge(X,Y)]).
true ;
false.*/



/*test : is_path2([edge(10,20),edge(10,20)]). true ;
false.*/

%test : is_path2([edge(X,Y)]). true



%pathBest :- [edges(No47854)] 



%ChatGPT
is_edge(edge(_,_)). %test :  is_edge(edge(1,3)). TRUE true
is_list_of_edges(L) :- is_list(L), maplist(is_edge,L).
%test :
%  is_list_of_edges([edge(1,3)]). TRUE true 
%  is_list_of_edges([point(1,3)]). FALSE false

%P-99 p61A only one element
%Head Left : P-99 p63
%is_path3([edge(1,3)]).
%without adjacence (adj in further version)
is_path3([E]) :- is_edge(E).
is_path3([edge(_,_)]). %vital lbl4586387
is_path3([edge(X,Y)]).
% unuseful  is_edge(E), is_edge(E2) because in 1st clause. No it would test only last one
%2 first http://www.cs.trincoll.edu/~ram/cpsc352/notes/prolog/search.html#:~:text=In%20Prolog%20list%20elements%20are%20enclosed%20by%20brackets%20and%20separated%20by%20commas.&text=Another%20way%20to%20represent%20a,with%20its%20first%20element%20removed. :
%= to access deeper, internal, embedded
%   E = edge(P1,P2) comparison
%   E is edge(P1,P2)
/*ERROR: Arguments are not sufficiently instantiated
ERROR: In:
ERROR:   [11] edge(1,3)is edge(_22800,_22802)
ERROR:   [10] is_path3([edge(1,3),...]) at /home/peter/POUB/swi-prolog-constraint-programming-csp-cop-intelligence-artificielle-AI/geodedicsSmallestPathFromedgesGPSlikeOK.pl:76
ERROR:    [9] toplevel_call(user:user: ...) at /usr/lib/swi-prolog/boot/toplevel.pl:1173*/
% :- edge(P1,P2), edge(P3,P4)....
%is_path3([ edge(P1,P2), edge(P3,P4)]).
is_path3([edge(P1,P2),edge(P3,P4)|Es]) :- is_path3([edge(P3,P4)|Es]).%is_edge(E), is_edge(E)%TODO test with E = edge()
% test  is_path3([edge(1, 3)]).          TRUE true
% test  is_path3([edge(1, 2)]).          TRUE true
% test  is_path3([edge(1,3),edge(1,3)]). TRUE in stage 0, FALSE FURTHER STAGE WITH ADJ true (only if  lbl4586387)) true
% test  is_path3([edge(1,2),edge(1,2)]).  TRUE in stage 0, FALSE FURTHER STAGE WITH ADJ true
% test  is_path3([edge(1,2),point(1,2)]). FALSE false
%test :  is_path3([edge(1,3),edge(1,3)]). TRUE  true


%recursive http://www.cs.trincoll.edu/~ram/cpsc352/notes/prolog/search.html#:~:text=In%20Prolog%20list%20elements%20are%20enclosed%20by%20brackets%20and%20separated%20by%20commas.&text=Another%20way%20to%20represent%20a,with%20its%20first%20element%20removed.
%loop(L) :- loop(L). %infinite
%test :  loop([1,2]).

%adjacence of edges
%is_path3_adj([edge(P1,P2),edge(P3,P4)|Es]) :- is_path3([edge(P1,P2),edge(P3,P4)|Es]),
%is_path3_adj([edge(_,_),edge(_,_)]).
%is_path3_adj([edge(P1,point(X,Y,No)),edge(point(X,Y,No),P4)]).
is_path3_adj([edge(P1,P25),edge(P25,P4)]).

% Es NON EMPTY :
is_path3_adj([edge(P1,point(X,Y,No)),edge(point(X,Y,No),P4)|Es]) :- is_path3([edge(P1,point(X,Y,No)),edge(point(X,Y,No),P4)|Es]),is_path3_adj([edge(point(X,Y,No),P4)|Es]).
%test :  is_path3_adj([edge(1,3),edge(1,3)]).  FALSE false,  FALSE because not adjacent
%test :  is_path3_adj([edge(1,2),edge(2,3)]).  TRUE true
%test :  is_path3_adj([edge(1,2),edge(2,4)]).  TRUE true

%test :  is_path3_adj([edge(1,9),edge(9,3)]).  TRUE true


%given data respectful
is_path4_adj_data([edge(P1,P25),edge(P25,P4)])  :- edge(P1,P25),edge(P25,P4).
is_path4_adj_data([edge(P1,point(X,Y,No)),edge(point(X,Y,No),P4)|Es]) :- is_path3([edge(P1,point(X,Y,No)),edge(point(X,Y,No),P4)|Es]),is_path4_adj_data([edge(point(X,Y,No),P4)|Es]).
%test :  is_path4_adj_data([edge(1,2),edge(2,3)]).  FALSE false  because not in my edge() data
%test :  is_path4_adj_data([edge(1,2),edge(2,4)]).  TRUE  true   because  in my edge() data
%test :  is_path4_adj_data([edge(1,2),edge(2,4)]).  TRUE  true   because  in my edge() data
%test :  is_path4_adj_data([edge(1,2),edge(3,4)]).  FALSE   

%test :  is_path4_adj_data([edge(1,9),edge(9,3)]).  FALSE false  because not in my edge() data (+ x axis backwards)

%TODO Object module property variables https://www.swi-prolog.org/pldoc/man?section=attvar lbl68110

%TODO DO NOT REPEAT = factorization = Single Source of Truth = functions =modularization -> Unit tests for non regression when refactor to factorization lbl68110


%TODO end_with_3
%     contents_only_3s


:- begin_tests(lists).
:- use_module(library(lists)).

test(path3_adj1) :-
    not(is_path3_adj([edge(1,3),edge(1,3)])).
test(path3_adj2) :-
    is_path3_adj([edge(1,2),edge(2,3)]).
test(path3_adj3) :-
    is_path3_adj([edge(1,2),edge(2,4)]).
test(path3_adj4) :-
    is_path3_adj([edge(1,9),edge(9,3)]). 


test(path4_adj_data1) :-
    not(is_path4_adj_data([edge(1,2),edge(2,3)])).					
test(path4_adj_data2) :-
    is_path4_adj_data([edge(1,2),edge(2,4)]).
test(path4_adj_data3) :-
    is_path4_adj_data([edge(1,2),edge(2,4)]).

test(path4_adj_data4) :-
    not(is_path4_adj_data([edge(1,2),edge(3,4)])).

test(path4_adj_data5) :-
    not(is_path4_adj_data([edge(1,9),edge(9,3)])).


test(edge_fx_ASC1OKasc) :-
    is_edge_fx_ASC(edge(point(0,0,1),point(10,9,4))).
test(edge_fx_ASC2KOdesc) :-
    not(is_edge_fx_ASC(edge(point(10,9,4),point(0,0,1)))).

:- end_tests(lists).

% is_edge_fx_ASC(E), label(E).
