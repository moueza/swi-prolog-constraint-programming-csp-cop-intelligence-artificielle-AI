%KO = clauses which evaluate to false = because no :- use_module(library(clpfd)).

%Matrix of costs ... cf Metanet https://github.com/moueza/isbn-0-8176-4009-6-engineering-and-scientific-computing-with-Scilab p112
%FRACTIONS ... resolution
%Uniform cost search ; A*
%https://www.youtube.com/watch?v=dRMvK76xQJI&ab_channel=JohnLevine
%automatic Dikstra 


%point (x,y,No6593)
%Point from		      
point(0,0,1).

point(1,1,2).
point(1,2,3).

point(10,9,4).
point(10,10,5). %Point end

%edge(PointNoFrom,PointNoTo,No47854)
edge(1,2).
edge(1,3).
edge(2,4).
edge(2,5).
edge(3,4).
edge(3,5).
%TODO scalar product x related

%P80 of P99		
%path :- [edges(No47854)]
is_path([edge(From,To)]).
is_path([H|L]) :- edge(From,To),is_path(L).

/*better def*/
is_path2([edge(From,To)]).
is_path2([edge(From,To)|L]) :-  is_path(L).

/*test : is_path([edge(10,20)]).
true ;
false.
      true because in general, not from my data */

/*test : is_path([edge(X,Y)]).
true ;
false.*/



/*test : is_path2([edge(10,20),edge(10,20)]). true ;
false.*/

%test : is_path2([edge(X,Y)]). true



%pathBest :- [edges(No47854)] 


%ChatGPT
is_list_of_edges(L) :- is_list(L), maplist(is_edge,L).
