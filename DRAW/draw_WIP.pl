dotted_straight_line(Point1,Point2,List_of_segments) :-
dotted_line_curved_arrow(Point1,Point2,List_of_segments)    

:- begin_tests(min_floats310).
test(minOK) :- min_floats(3.0, 1.0,1.0).
test(minKO) :- not(min_floats(3.0, 1.0,99.0)).
:- end_tests(min_floats310).
